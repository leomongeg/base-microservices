#!/bin/sh
##
# Script to just undeploy the MongoDB Service & StatefulSet but nothing else.
##

# Just delete mongod stateful set + mongodb service onlys (keep rest of k8s environment in place)

kubectl delete services base-moleculer
kubectl delete deployment base-moleculer

# Show persistent volume claims are still reserved even though mongod stateful-set has been undeployed
kubectl get all
