#!/bin/sh
##
# Script to deploy a Kubernetes project with a StatefulSet running a App Node
##

# Create Node service
kubectl apply -f ../resources/node-service.yaml --validate=false
sleep 5

# Print current deployment state (unlikely to be finished yet)
kubectl get all 
kubectl get persistentvolumes
echo
echo "Keep running the following command until all pods are shown as running:  kubectl get all"
echo
