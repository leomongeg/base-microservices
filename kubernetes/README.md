
# Base Microservices Deploy Kubernetes

### [Kubernetes](https://kubernetes.io/docs/concepts/overview/what-is-kubernetes/)  is an open-source system for automating deployment, scaling, and management of containerized applications.

This is a base project to create a microservice in Kubernetes. It was create a base of:

 - [ ] [Node.js and MongoDB on Kubernetes](https://github.com/kubernetes/examples/tree/master/staging/nodesjs-mongodb)

- [ ] [MongoDB Deployment Demo for Kubernetes on Minikube](https://github.com/pkdone/minikube-mongodb-demo)

- [ ] [Web UI (Dashboard)](https://kubernetes.io/docs/tasks/access-application-cluster/web-ui-dashboard/) * Optional
  

![kubernetes](https://d1.awsstatic.com/PAC/kuberneteslogo.eabc6359f48c8e30b7a138c18177f3fd39338e05.png)

  

## 1 How To Run

### 1.1 Prerequisites

Ensure the following dependencies are already fulfilled on your host Linux/Windows/Mac Workstation/Laptop:

1. The [Docker Kubernetes](https://www.docker.com/products/orchestration).

2. The [Project Base Transport](https://bitbucket.criticalmass.com/users/joseg/repos/base-microservices-transport/browse) working with Kubernetes.

### 1.2 Main Deployment Steps

  

### 1.2.1 MongoDB

1. To deploy the MongoDB Service (including the StatefulSet running "mongod" containers), via a command-line terminal/shell, execute the following:

```

$ cd kubernetes/scripts/scripts-mongo

$ ./generate.sh

```

2. Re-run the following command, until all 2 “mongod” pods (and their containers) have been successfully started (“Status=Running”; usually takes a minute or two).

```

$ kubectl get all

```

3. Execute the following script which connects to the first Mongod instance running in a container of the Kubernetes StatefulSet, via the Mongo Shell, to (1) initialise the MongoDB Replica Set, and (2) create a MongoDB admin user (specify the password you want as the argument to the script, replacing **'abc123'**).

```

$ ./configure_repset_auth.sh abc123

```

You should now have a MongoDB Replica Set initialised, secured and running in a Kubernetes StatefulSet.

#### 1.2.1.1 Example Tests To Run To Check Things Are Working

Use this section to prove:

1. Data is being replicated between members of the containerised replica set.

2. Data is retained even when the MongoDB Service/StatefulSet is removed and then re-created (by virtue of re-using the same Persistent Volume Claims).

#### 1.2.1.2 Replication Test

Connect to the container running the first "mongod" replica, then use the Mongo Shell to authenticate and add some test data to a database:

$ kubectl exec -it mongod-0 -c mongod-container bash

$ mongo

> db.getSiblingDB('admin').auth("main_admin", "abc123");

> use test;

> db.testcoll.insert({a:1});

> db.testcoll.insert({b:2});

> db.testcoll.find();

Exit out of the shell and exit out of the first container (“mongod-0”). Then connect to the second container (“mongod-1”), run the Mongo Shell again and see if the previously inserted data is visible to the second "mongod" replica:

$ kubectl exec -it mongod-1 -c mongod-container bash

$ mongo

> db.getSiblingDB('admin').auth("main_admin", "abc123");

> db.setSlaveOk();

> use test;

> db.testcoll.find();

You should see that the two records inserted via the first replica, are visible to the second replica.

#### 1.2.1.3 Redeployment Without Data Loss Test

To see if Persistent Volume Claims really are working, run a script to drop the Service & StatefulSet (thus stopping the pods and their “mongod” containers) and then a script to re-create them again:

$ ./delete_service.sh

$ ./recreate_service.sh

$ kubectl get all

As before, keep re-running the last command above, until you can see that all 3 “mongod” pods and their containers have been successfully started again. Then connect to the first container, run the Mongo Shell and query to see if the data we’d inserted into the old containerised replica-set is still present in the re-instantiated replica set:

$ kubectl exec -it mongod-0 -c mongod-container bash

$ mongo

> db.getSiblingDB('admin').auth("main_admin", "abc123");

> use test;

> db.testcoll.find();

You should see that the two records inserted earlier, are still present.

#### 1.2.1.4 Undeploying & Cleaning Down the Kubernetes Environment

Run the following script to undeploy the MongoDB Service & StatefulSet.

$ ./teardown.sh

### 1.2.2 Node JS

1. To deploy the Node JS Service (including the Deployment running "base-moleculer" containers), via a command-line terminal/shell, execute the following:

```

$ cd kubernetes/scripts/scripts-node

$ ./generate.sh

```

2. Re-run the following command, until all “base-moleculer” pods (and their containers) have been successfully started (“Status=Running”; usually takes a minute or two).

```

$ kubectl get all

```

You should now have a Node JS Deployment & Service initialised, secured and running in a Kubernetes StatefulSet.

#### 1.2.2.1 Undeploying & Cleaning Down the Kubernetes Environment

Run the following script to undeploy the NodeJs Deployment & Service.

$ ./teardown.sh