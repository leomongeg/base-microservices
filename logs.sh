#!/usr/bin/env bash
source bash/load_env.sh
source bash/print_style.sh

if [[ $# -eq 0 ]] ; then
    container="moleculer"
else
    container="$1"
fi

print_style "Show the logs of ${container}: \n" "info"

docker-compose logs -t -f ${container}