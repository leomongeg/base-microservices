#!/usr/bin/env bash
source bash/print_style.sh

print_style "Loading the .env files: \n\n" "info"
export $(grep -v '^#' docker/.env | xargs -0)
export $(grep -v '^#' .env | xargs -0)