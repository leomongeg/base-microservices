/**
 * base-microservices -
 *
 * Initial version by: leonardom
 * Initial version created on: 8/3/18
 */
import { Service } from "typedi";
import { IService } from "./interface/IService";
import { UserRepository } from "../repositories/UserRepository";
import { InjectRepository } from "typeorm-typedi-extensions";
import { User } from "../entity/User";
import { getRepository } from "typeorm";

const activeDirectory: any = require("activedirectory");

@Service()
export class ActiveDirectoryService implements IService {

    private client: any;

    @InjectRepository(User)
    private userRepo: UserRepository;

    constructor() {
        this.createClient();
    }

    private createClient(): void {
        const options = this.getConfigOptions();
        this.client = new activeDirectory(options);
    }

    private getConfigOptions(): object {

        if (!process.env.AD_BASE_DN || !process.env.AD_URL || !process.env.AD_USERNAME || !process.env.AD_PASSWORD) {
            this.usageTip();
            process.exit(1);
            return undefined;
        }

        return {
            baseDN: process.env.AD_BASE_DN,
            password: process.env.AD_PASSWORD,
            url: process.env.AD_URL,
            username: process.env.AD_USERNAME
        };
    }

    usageTip(): void {
        console.log("~~~~~~~~~~");
        console.log("Active Directory Connection");
        console.log("The Active Directory connection settings are missing in your ENV File:");
        console.log("AD_BASE_DN, AD_URL, AD_USERNAME, AD_PASSWORD");
        console.log("~~~~~~~~~~");
    }

    /**
     * Find LDAP user by email.
     *
     * @param {string} email
     * @returns {Promise<any>}
     */
    findUser(email: string): Promise<any> {
        return new Promise<any>(((resolve) => {
            this.client.findUser(email, (err: any, user: any) => {
                if (err) {
                    // @TODO invoke exception reporting
                    console.log("ERROR: " + JSON.stringify(err));
                    resolve(undefined);
                    return;
                }

                resolve(user);
            });
        }));
    }
}