/**
 * base-microservices -
 *
 * Initial version by: leonardom
 * Initial version created on: 8/9/18
 */
import { Service } from "typedi";
import { IService } from "../interface/IService";
import { UserRepository } from "../../repositories/UserRepository";

const activeDirectory: any = require("activedirectory");

@Service()
export class ActiveDirectoryService implements IService {

    private client: any;

    private userRepo: UserRepository;

    constructor() {
        this.createClient();
    }

    private createClient(): void {
    }

    usageTip(): void {
    }

    /**
     * Find LDAP user by email.
     *
     * @param {string} email
     * @returns {Promise<any>}
     */
    async findUser(email: string): Promise<any> {
        const users: any[] = [
            {
                email: "user001@thehangar.cr",
                name: "User 001"
            },
            {
                email: "user002@thehangar.cr",
                name: "User 002"
            },
            {
                email: "user003@thehangar.cr",
                name: "User 003"
            }
        ];

        return users.find(user => user.email == email);
    }
}