/**
 * base-microservices -
 *
 * Initial version by: leonardom
 * Initial version created on: 8/3/18
 */

export class IService {

    /**
     * Use this function to alert the user how to use or define the access token in the env file.
     */
    usageTip ?: () => void;
}