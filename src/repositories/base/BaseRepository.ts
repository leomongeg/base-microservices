/**
 * base-microservices -
 *
 * Initial version by: leonardom
 * Initial version created on: 8/7/18
 */

import { MongoEntityManager, Repository } from "typeorm";

export class BaseRepository<Entity> extends Repository<Entity> {

    public getEntityManager(): MongoEntityManager {
        return <MongoEntityManager> this.manager;
    }

}