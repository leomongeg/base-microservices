/**
 * base-microservices -
 *
 * Initial version by: leonardom
 * Initial version created on: 8/6/18
 */
import { EntityRepository, Repository } from "typeorm";
import { User } from "../entity/User";
import { BaseRepository } from "./base/BaseRepository";

@EntityRepository(User)
export class UserRepository extends BaseRepository<User> {

    /**
     * Find User by email.
     *
     * @param {string} email
     * @returns {Promise<User>}
     */
    findByEmail(email: string): Promise<User> {
        return this.findOne({ email: email });
    }

    /**
     * This is a demo method to demostrates the use of Repository Classes.
     *
     * @param adUser
     */
    async registerUser(adUser: any): Promise<User> {
        const exist = await this.findByEmail(adUser.mail);

        if (exist) return exist;

        const user    = new User();
        user.email    = adUser.mail;
        user.fullname = adUser.displayName;
        await this.save(user, {listeners: true});

        return user;
    }
}