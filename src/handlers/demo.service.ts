/**
 * base-microservices -
 *
 * Initial version by: leonardom
 * Initial version created on: 8/4/18
 */
import { Action, Context, ServiceBroker } from "moleculer";
import { ActiveDirectoryService } from "../services/ActiveDirectoryService";
import { Container } from "typedi";
import { UserRepository } from "../repositories/UserRepository";
import { BaseService } from "../base/BaseService";
import { User } from "../entity/User";

class DemoService extends BaseService {

    private adClient: ActiveDirectoryService;

    constructor(broker: ServiceBroker) {
        super(broker);

        this.name = "demo";
        this.actions = {
            "add": <Action> {
                handler: this.add,
                params: {
                    email: "string"
                }
            },
            "findByEmail": <Action> {
                handler: this.findByEmail,
                params: {
                    email: "string"
                }
            }
        };

        this.parseServiceSchema({
            actions: this.actions,
            created: this.createdService,
            dependencies: [
                "orm"
            ],
            name: this.name,
            started() {
                return this.serviceStarted();
            }
        });

    }

    private async serviceStarted(): Promise<void> {
        this.adClient = Container.get(ActiveDirectoryService);
    }

    private getUserRepository(): UserRepository {
        return <UserRepository> this.getRepository(UserRepository);
    }

    /**
     * Action add user from LDAP.
     *
     * @param ctx
     */
    public async add(ctx: Context): Promise<User> {
        const adUser = await this.adClient.findUser(ctx.params.email);
        const user = await this.getUserRepository().registerUser(adUser);
        // const user = await this.userRepo.registerUser(adUser);

        this.broker.emit("demo.added", user);

        return user;
    }

    /**
     * Action find user by email.
     *
     * @param ctx
     */
    public async findByEmail(ctx: Context): Promise<User> {
        return this.getUserRepository().findByEmail(ctx.params.email);
    }

    createdService() {
        console.log("Service CREATED");
    }
}

export = DemoService;