/**
 * base-microservices -
 *
 * Initial version by: leonardom
 * Initial version created on: 8/4/18
 */

import { Service, ServiceBroker } from "moleculer";
import { ConnectionOptions, createConnection, useContainer as ormUseContainer } from "typeorm";
import { Container } from "typedi";
import { registeredEntities } from "../entity/loader/entityLoader";


class OrmService extends Service {

    constructor(broker: ServiceBroker) {
        super(broker);

        this.checkForDBConfig();
        this.parseServiceSchema({
            created: this.serviceCreated,
            name: "orm",
            started() {
                return this.createMongoConnection();
            }
        });
    }

    private getConfigDevelopment(): ConnectionOptions {
        return <ConnectionOptions> {
            "cli": {
                "entitiesDir": "src/entity",
                "migrationsDir": "src/migration",
                "subscribersDir": "src/subscriber"
            },
            "database": process.env.MONGODB_NAME,
            "entities": registeredEntities,
            "host": process.env.MONGODB_HOST,
            "logging": true,
            "migrations": [
                "src/migration/*.js"
            ],
            "port": Number(process.env.MONGODB_PORT),
            "subscribers": [
                "src/subscriber/*.js"
            ],
            "synchronize": true,
            "type": "mongodb"
        };
    }

    private getConfigProduction(): ConnectionOptions {
        return <ConnectionOptions> {
            "authSource": "admin",
            "cli": {
                "entitiesDir": "src/entity",
                "migrationsDir": "src/migration",
                "subscribersDir": "src/subscriber"
            },
            "database": process.env.MONGODB_NAME,
            "entities": registeredEntities,
            "host": process.env.MONGODB_HOST,
            "logging": true,
            "migrations": [
                "src/migration/*.js"
            ],
            "password": process.env.MONGODB_PASSWORD,
            "port": Number(process.env.MONGODB_PORT),
            "replicaSet": "MainRepSet",
            "subscribers": [
                "src/subscriber/*.js"
            ],
            "synchronize": true,
            "type": "mongodb",
            "username": process.env.MONGODB_USERNAME
        };
    }

    private getConfigConnection(): ConnectionOptions {
        if (process.env.APP_ENV.toLowerCase() === "prod" || process.env.APP_ENV.toLowerCase() === "production")
            return this.getConfigProduction();

        return this.getConfigDevelopment();
    }

    private async createMongoConnection(): Promise<any> {
        console.log("Creating MongoDB connection");
        ormUseContainer(Container);
        return new Promise<any>(resolve => {

            createConnection(this.getConfigConnection()).then(async (connection) => {
                this.broker.broadcast("orm.connected");
                resolve();
            });
        });
    }

    private checkForDBConfig(): void {
        if (!process.env.MONGODB_NAME || !process.env.MONGODB_HOST || !process.env.MONGODB_PORT) {
            console.log("~~~~~~~~~~");
            console.log("MongoDB Connection");
            console.log("The MongoDB connection settings are missing in your ENV File:");
            console.log("Expected: MONGODB_HOST, MONGODB_NAME, MONGODB_PORT");
            console.log("~~~~~~~~~~");
            throw TypeError("The MongoDB connection settings are missing in your ENV File");
        }
    }
}

export = OrmService;