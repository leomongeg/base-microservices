/**
 * base-microservices -
 *
 * Initial version by: leonardom
 * Initial version created on: 8/4/18
 */
import { BaseService } from "../base/BaseService";
import { Actions, ServiceBroker, ServiceEvents } from "moleculer";


class FooService extends BaseService {
    constructor(broker: ServiceBroker) {
        super(broker);

        this.name = "foo";
        this.actions = <Actions> {};

        this.events = <ServiceEvents> {
            "demo.added": this.demoAdded,
            "orm.connected": this.onORMConnected
        };

        this.parseServiceSchema({
            actions: this.actions,
            events: this.events,
            name: this.name
        });
    }

    /**
     * Example of balanced event subscribe.
     *
     * @param value
     */
    demoAdded(value: any) {
        this.logger.info("Demo User added Event");
        this.logger.info(value);
    }

    /**
     * Example of broadcast event subscribe.
     *
     * @param payload
     */
    private onORMConnected(payload: any) {
        this.logger.info("FOO On ORM Connected");
    }
}

export = FooService;