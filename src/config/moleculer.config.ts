export interface Configuration {
    namespace: string;
    transporter: string;
    logger: boolean;
    logLevel: string;
    logFormatter: string;
    cacher: Cacher;
    metrics: boolean;
}
export interface Cacher {
    type: string;
    options: Options;
}
export interface Options {
    maxParamsLength: number;
}

const configuration: Configuration = {
    cacher: {
        options: {
            maxParamsLength: 100
        },
        type: "memory"
    },
    logFormatter: "short",
    logLevel: "info",
    logger: true,
    metrics: true,
    namespace: "conduit",
    transporter: process.env.TRANSPORTER
};

export default configuration;