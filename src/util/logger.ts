import { Logger, transports } from "winston";
import { ENVIRONMENT } from "./secrets";

const logger = new (Logger)({
    transports: [
        new (transports.Console)({ level: process.env.NODE_ENV === "production" ? "error" : "debug" }),
        new (transports.File)({ filename: "debug.log", level: "debug"})
    ]
});

if (process.env.NODE_ENV !== "production") {
    logger.debug("Logging initialized at debug level");
}

export default logger;

