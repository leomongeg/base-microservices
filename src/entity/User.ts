/**
 * base-microservices -
 *
 * Initial version by: leonardom
 * Initial version created on: 8/6/18
 */

import { Column, CreateDateColumn, Entity, ObjectID, ObjectIdColumn, VersionColumn } from "typeorm";


@Entity()
export class User {

    @ObjectIdColumn()
    id: ObjectID;

    @Column()
    email: string;

    @Column()
    fullname: string;

    @VersionColumn()
    version: number;

    @CreateDateColumn()
    createdAt: Date;
}