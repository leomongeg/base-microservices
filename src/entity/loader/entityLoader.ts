/**
 * base-microservices -
 *
 * Initial version by: leonardom
 * Initial version created on: 6/14/18
 */
import { User } from "../User";

export const registeredEntities = [
    User
];