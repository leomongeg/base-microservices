/**
 * base-microservices -
 *
 * Initial version by: leonardom
 * Initial version created on: 8/9/18
 */

import { ServiceBroker } from "moleculer";
import DemoService from "../../handlers/demo.service";
import OrmService from "../../handlers/orm.service";
import { User } from "../../entity/User";
import dotenv from "dotenv";


// Example how import mocked class with Je
// jest.mock("../../services/ActiveDirectoryService");

describe("Demo Service Handler", () => {
    dotenv.config({path: ".env"});

    const broker = new ServiceBroker();
    const email = "leonardom@thehangar.cr";

    broker.createService(OrmService);
    broker.createService(DemoService);

    beforeAll(() => broker.start());
    afterAll(() => broker.stop());

    describe("Test 'demo.add' Action", () => {
        it("Should return an User object", async () => {
            const user = await broker.call("demo.add", { email: email });
            expect(user).toBeInstanceOf(User);
        });

        it("Should return an valid email value", async () => {
            const user = await broker.call("demo.add", { email: email });
            expect(user.email).toBe(email);
        });
    });

    describe("Test 'demo.findByEmail' Action", () => {
        it("Should return an User object", async () => {
            const user = await broker.call("demo.findByEmail", { email: email });
            expect(user).toBeInstanceOf(User);
        });
    });

});