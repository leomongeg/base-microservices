import dotenv from "dotenv";
import { ServiceBroker } from "moleculer";
import OrmService = require("../../handlers/orm.service");
import { Connection, getMongoManager } from "typeorm";

/**
 * base-microservices -
 *
 * Initial version by: leonardom
 * Initial version created on: 8/10/18
 */

describe("ORM Service Handler", () => {

    describe("Test 'request db config properties'", () => {
        beforeAll(() => {
           delete process.env.MONGODB_NAME;
           delete process.env.MONGODB_HOST;
        });

        const broker = new ServiceBroker();

        it("Should exit and print console error", () => {
            expect(() => { broker.createService(OrmService); }).toThrow(TypeError);
        });
    });

    describe("Test 'orm db connection'", () => {

        dotenv.config({path: ".env"});
        const broker = new ServiceBroker();
        let connection: Connection;

        broker.createService(OrmService);
        beforeAll(() => broker.start());
        afterAll(() => broker.stop());

        it("Should create a DB connection", () => {
            connection = getMongoManager().connection;
            expect(connection).toBeInstanceOf(Connection);
        });
    });
});