/**
 * base-microservices -
 *
 * Initial version by: leonardom
 * Initial version created on: 8/5/18
 */

import { Service, ServiceBroker, ServiceEvents, ServiceSchema } from "moleculer";
import { ObjectType } from "../../node_modules/typeorm/common/ObjectType";
import { getMongoManager } from "typeorm";

export class BaseService extends Service {

    public events: ServiceEvents;

    constructor(broker: ServiceBroker, schema?: ServiceSchema) {
        super(broker, schema);
    }

    /**
     * Get Custom repository from the MongoManagerInstance.
     * Example: const userRepo = this.getRepository(UserRepository);
     *
     * @param customRepository
     */
    protected getRepository<T>(customRepository: ObjectType<T>): T {
        return getMongoManager().getCustomRepository(customRepository);
    }
}