#!/usr/bin/env bash
source bash/print_style.sh
source bash/load_env.sh

if [[ $# -eq 0 ]] ; then
    print_style "Missing arguments { Tag }.\n" "danger"
    display_options
    exit 1
fi

TAG="$1"
BUILD="thicorporate/node-base-microservices"

echo "\n"
print_style "Creating Tag: ${TAG}.\n" "info"
docker tag ${BUILD} ${BUILD}:${TAG}

echo "\n"
print_style "Creating Build Image ${BUILD}:${TAG}.\n" "info"
docker build -t ${BUILD}:${TAG} -f docker/node_production/Dockerfile .

echo "\n"
print_style "Pushing Image ${BUILD}:${TAG}.\n" "info"
docker push ${BUILD}:${TAG}

echo "\n"
print_style "Ready ${BUILD}:${TAG}.\n" "success"

# http://patorjk.com/software/taag/#p=display&f=Big&t=Happy%20Codding%20!%20!%20!
echo "  _____                _         _____            _         "
echo " |  __ \              | |       |  __ \          | |        "
echo " | |__) |___  __ _  __| |_   _  | |__) |___   ___| | _____  "
echo " |  _  // _ \/ _  |/ _  | | | | |  _  // _ \ / __| |/ / __| "
echo " | | \ \  __/ (_| | (_| | |_| | | | \ \ (_) | (__|   <\__ \ "
echo " |_|  \_\___|\__,_|\__,_|\__, | |_|  \_\___/ \___|_|\_\___/ "
echo "                          __/ |                             "
echo "                         |___/                            \n"
