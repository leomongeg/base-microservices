# Base Microservices with MoleculerJS - TypeScript Project


## Motivation

This base project for microservices with Moleculerjs and TypeScript, helps to build microservices using techniques like 
Dependency Injection and other well know development patterns like Repositories and full object-oriented 
project stack.

The main idea is to use a well-structured class for our microservice instead use a plain object how the documentation 
suggests. And another important point is to define a well-structured project stack that easily allows developers 
to group the business logic into Dependency Injection Services and Repositories for the data access Layer.



### Get Started

Clone this repository:

`git clone https://bitbucket.org/leomongeg/base-microservices.git`

Install dependencies:

This project requires node version 8.9.4. You can use [NVM](https://github.com/creationix/nvm) to select the correct 
version of node. 

```
cd base-microservice
nvm use
npm install
```

Create a .env file, copy the .env.example file and rename to .env and setup your DB configuration to the .env file.

Then run the following command in your command line:

```
npm run build
```

Run the project from the command line for development:

```
npm run debug
```

Ever that you want to run the compiler and linters run `npm run build`.

Once that you execute the `npm run debug` task, the project is ready to watch for changes in you TypeScript files to be 
compiled for debugging in real time.

For development process you need to start Moleculer in order to your microservices start receiving requests. To do that, 
you need to execute the following command in a separate terminal window:

```bash
npm run moleculer
```  

the above node task, startup the Moleculer project and start the Moleculer CLI console for development, in order that 
you can interact with your microservices. 

###Project Stack

In order to understand the project stack is important to understand:

- [What is a Microservice?](https://microservices.io/)
- [What is Moleculer JS?](https://moleculer.services/docs)
- [What is Dependency Injection?]((docs/dependency_injection.md))
- [What is the Repository pattern?](docs/model_repositories.md)

Once you have the basic knowledge basis you are able to understand the [project stack](docs/project_stack.md). So please, continue to the next 
step that is [Understanding the Project Stack.](docs/project_stack.md)

### Documentation

Please read this docs in order to start development:

- [Dependency Injection](docs/dependency_injection.md)
- [Create model files and Repository pattern](docs/model_repositories.md)
- [Microservices io](https://microservices.io/)
- [Moleculer JS](https://moleculer.services/docs)


### Docker

Docker hasn't been succesfuly configured for this project.
Right now, you can:
- use your local Mongo/Node.js or
- run Mongo on the container and run the node server locally.
To run only the Mongo container:
`docker-compose up -d mongodb`


### TODO:

- Testing
- Uncaught exception handler

### About PDSA Catalog

Built with love by The Hangar Corporate Team.