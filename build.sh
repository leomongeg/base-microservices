#!/usr/bin/env bash
source bash/print_style.sh

print_style "Copying the .env files: \n" "info"

cp .env.example .env
cp docker/.env.example docker/.env

source bash/load_env.sh

print_style "Installing node modules8: \n" "info"

nvm use
npm install

print_style "Init the build of docker: \n" "info"

docker-compose build
