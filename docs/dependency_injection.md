# Base Microservice Project

## Dependency Injection

Dependency injection is a technique whereby one object (or static method) supplies the dependencies of another object. A dependency is an object that can be used (a service). An injection is the passing of a dependency to a dependent object (a client) that would use it. The service is made part of the client's state.[1] Passing the service to the client, rather than allowing a client to build or find the service, is the fundamental requirement of the pattern.

For the HR Slack Bot, we use [TypeDI](https://github.com/typestack/typedi) that is a dependency injection tool for Javascript and Typescript. Is very easy and well structured.

Here is the documentation that shows you how to use the dependency injection: https://github.com/typestack/typedi

Here is a quick view for Typescript taken of the TypeDI docs; he most simple usage example is::

```typescript
import "reflect-metadata";
import {Service, Container} from "typedi";

@Service()
class SomeClass {

    someMethod() {
    }

}

let someClass = Container.get(SomeClass);
someClass.someMethod();
```

Then you can call Container.get(SomeClass) from anywhere in your application and you'll always have the same instance of SomeClass.

You can use property injection and inject services into your class using @Inject decorator:

```typescript
import {Container, Inject, Service} from "typedi";

@Service()
class BeanFactory {
    create() {
    }
}

@Service()
class SugarFactory {
    create() {
    }
}

@Service()
class WaterFactory {
    create() {
    }
}

@Service()
class CoffeeMaker {

    @Inject()
    beanFactory: BeanFactory;
    
    @Inject()
    sugarFactory: SugarFactory;
    
    @Inject()
    waterFactory: WaterFactory;

    make() {
        this.beanFactory.create();
        this.sugarFactory.create();
        this.waterFactory.create();
    }

}

let coffeeMaker = Container.get(CoffeeMaker);
coffeeMaker.make();
```

You can also use a constructor injection:

```typescript
import {Container, Service} from "typedi";

@Service()
class BeanFactory {
    create() {
    }
}

@Service()
class SugarFactory {
    create() {
    }
}

@Service()
class WaterFactory {
    create() {
    }
}

@Service()
class CoffeeMaker {

    constructor(private beanFactory: BeanFactory,
                private sugarFactory: SugarFactory,
                private waterFactory: WaterFactory) {}

    make() {
        this.beanFactory.create();
        this.sugarFactory.create();
        this.waterFactory.create();
    }

}

let coffeeMaker = Container.get(CoffeeMaker);
coffeeMaker.make();
```

With TypeDI you can use a named services. Example:

```typescript
import {Container, Service, Inject} from "typedi";

interface Factory {
    create(): void;
}

@Service("bean.factory")
class BeanFactory implements Factory {
    create() {
    }
}

@Service("sugar.factory")
class SugarFactory implements Factory {
    create() {
    }
}

@Service("water.factory")
class WaterFactory implements Factory {
    create() {
    }
}

@Service("coffee.maker")
class CoffeeMaker {

    beanFactory: Factory;
    sugarFactory: Factory;

    @Inject("water.factory")
    waterFactory: Factory;

    constructor(@Inject("bean.factory") beanFactory: BeanFactory,
                @Inject("sugar.factory") sugarFactory: SugarFactory) {
        this.beanFactory = beanFactory;
        this.sugarFactory = sugarFactory;
    }

    make() {
        this.beanFactory.create();
        this.sugarFactory.create();
        this.waterFactory.create();
    }

}

let coffeeMaker = Container.get<CoffeeMaker>("coffee.maker");
coffeeMaker.make();
```