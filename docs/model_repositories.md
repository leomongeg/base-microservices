# Base Microservice Project

## Create Entities and Entity Repositories

This project uses [TypeORM](https://github.com/typeorm/typeorm) as ORM in this case ODM, because we use MongoDB as 
principal DB engine. So we defined a structure to create the model classes that we called entities and the repositories 
that we use to define the DAO logic layer for our application.

### How to create an Entity Class

Inside the project structure, you can find a project_root}/src/entity directory. Inside the entity directory, you can 
declare your Entity model classes. The Entity model class defines the structure of your documents inside the MongoDB 
and the same time is used as schema files for the ORM to define the document's structure.

Let's see how to declare an Entity class:

```typescript

import { BeforeInsert, Column, CreateDateColumn, Entity, ObjectID, ObjectIdColumn, VersionColumn } from "typeorm";

@Entity() // This entity decorator tells to the ORM that this is an entity
export class User {

    @ObjectIdColumn() // This decorator defines an ObjectId type inside MongoDB
    id: ObjectID;

    @Column() // This is a decorator that tells to the ORM that this is a column inside MongoDB, the ORM takes the var type to define the type inside Mongo, in this case, String type.
    firstName: string;

    @Column()
    lastName: string;

    @Column()
    email: string;
    
    @Column()
    password: string;

    @CreateDateColumn()
    createdAt: Date;

    @Column()
    active: Boolean;

    @VersionColumn()
    version: number;

    @BeforeInsert() // This is an event decorator. Those events are triggered by the ORM lifecycle process.
    updateDates() {
        this.createdAt = new Date();
    }
    
    encryptPassword(plainPass: string): void {
        this.password = passGenator(plainPass);
    }
    
}

```  

As you can see is very easy to declare an entity class you can define functions inside this class but these functions 
should represent logic for this class and not DAO business logic, for example `encryptPassword`.

Once you created the class you need to register it in order the ORM can load your class. To register your class
you need to go to [{project_root}/src/entity/loader/entityLoader.ts](../src/entity/loader/entityLoader.ts)

For more information about Class Decorators see the [TypeORM Decorators Reference](https://github.com/typeorm/typeorm/blob/master/docs/decorator-reference.md)

### How to create Entity Repository classes

Repository pattern, it provides an abstraction of data, so that your application can work with a simple abstraction 
that has an interface approximating that of a collection. Adding, removing, updating, and selecting items from this 
collection is done through a series of straightforward methods, without the need to deal with database concerns like 
connections, commands, cursors, or readers. Using this pattern can help achieve loose coupling and can keep domain 
objects persistence ignorant.

How to create a Repository Class, the main idea is create a repository class related to one Entity class. TypeORM
defines a foundation to create repositories so is very easy.

- Create you class called as the name of your entity class and the word Repository for example `UserRepository`.
- Extends you repository class from typeorm/Repository<T> and set the generic type in this case `<User>` 
- Add the proper decorators

Let's see the example:

```typescript
import { EntityRepository, Repository } from "typeorm";
import { User } from "../entity/User";


@EntityRepository(User) // This decorator tells to the ORM that this is a Repository and register it to the DI container (Dependency Injection)
export class DemoRepository extends Repository<User> {

    /**
     * @TODO: Your custom DAO logic goes here.
     * Example:
     */
    myFancyFindAll(): Promise<User[]> {
        return this.find();
    }
}
```

and that's it to call your repository classes you most call the DI container to access to your Repos.