
# Docker Yeah!!! :whale2:

This is the configuration of the project, using Docker Compose.

## Requirements:

* Download and install [Docker](https://docs.docker.com/compose/install/)

* **Clone and init the Project**: [base-microservices-transport](https://bitbucket.criticalmass.com/users/joseg/repos/base-microservices-transport/browse)  :star:

## Set Up

Run on the terminal on root folder of proyect (Recommended):

```$ sh build.sh install ```

### Start Docker and Proyect: ###

* Option A: run on the terminal on root folder of proyect (Recommended):

```$ sh start.sh up```

* Option B: run on the terminal on root folder of proyect:

```$ cd docker && docker-compose up -d ```

### Stop Docker and Proyect: ###

* Option A: run on the terminal on root folder of proyect (Recommended):

```$ sh stop.sh up```

* Option B: run on the terminal on root folder of proyect:

```$ cd docker && docker-compose down ```

### Show logs of moleculer Docker: ###

* Option A: run on the terminal on root folder of proyect (Recommended):

```$ sh logs.sh up```

* Option B: run on the terminal on root folder of proyect:

```$ cd docker && docker-compose logs -t -f moleculer ```

### Connect to REPL moleculer Docker [#](https://github.com/moleculerjs/moleculer-repl#repl-module-for-moleculer-): ###

#### IMPORTANT ####  

**THE PROJECT Project**: [base-microservices-transport](https://bitbucket.criticalmass.com/users/joseg/repos/base-microservices-transport/browse) **is required**

* Option A: run on the terminal on root folder of proyect (Recommended):

```$ sh connect.sh up```

* Option B: run on the terminal on root folder of proyect:

```$ cd docker && docker-compose exec moleculer moleculer connect amqp://rabbitmq:5672 ```

**Note**: The ${TRANSPORTER} can change if you manipulate the .env file on docker folder.

### Link ###

| Service | Link |
| ------------- | -------------------------------------------------------- |
| Rabbit MQ | [http://localhost:15672/](http://localhost:8080/) User: guest - pass: guest|