#!/usr/bin/env bash
source bash/load_env.sh
source bash/print_style.sh

print_style "Preparing to connect\n" "info"
docker-compose exec moleculer moleculer connect ${TRANSPORTER}