#!/usr/bin/env bash
source bash/print_style.sh
source bash/load_env.sh

print_style "Init Backup database ${MONGODB_DATABASE}: \n" "info"

docker-compose exec mongodb mongodump -d ${MONGODB_DATABASE} -o /backup

print_style "Done \n\n" "info"